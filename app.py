# quick project, quick solution
from flask import Flask, render_template, request
import urllib.request, json 
import traceback

app = Flask(__name__)

MLB_domain = "https://statsapi.mlb.com"
team_endpoint = "/api/v1/teams?sportId=1"
single_team = "/api/v1/teams/"
player = "/api/v1/people/"
team_logo = "https://www.mlbstatic.com/team-logos/"
player_head_shot = "https://securea.mlb.com/mlb/images/players/head_shot/"
player_bio = "?hydrate=stats(group=[hitting,pitching,fielding],type=[yearByYear])"
svg = ".svg"
jpg = ".jpg"
roster = "/roster"

def get_url(url):
    with urllib.request.urlopen(url) as url:
        return json.loads(url.read().decode())

def get_teams(uri):
    data = get_url(uri)
    return data['teams']

def get_team_roster(uri):
    data = get_url(uri)
    return data['roster']

def get_player(uri):
    data = get_url(uri)
    return data['people'][0]

def get_team_info(teams):
    if teams == []:
        return []
    team_info = []
    for d in teams:
        team_info.append({'link': "/team?id=" + str(d['id']), 'name': d['name'], 'logo': team_logo + str(d['id']) + svg})
    return team_info

def get_player_info(players):
    player_info = []
    for p in players:
        player_info.append({"id": p["person"]["id"], "photo": player_head_shot + str(p["person"]["id"]) + jpg,
        "link": "/player?id=" + str(p["person"]["id"]), "name": p["person"]["fullName"], "jerseyNumber": p["jerseyNumber"], 
        "position_name": p["position"]["name"], "position_type": p["position"]["type"], "status": p["status"]["description"]})
    return player_info

def get_player_bio(player):
    playerBio = {"teamId": "", "photo": player_head_shot + str(player["id"]) + jpg, "name": player["fullName"], "age": player["currentAge"], 
    "height": player["height"], "weight": player["weight"], "birthcity": player.get("birthCity", "N/A"), "birthcountry": player.get("birthCountry", "N/A"), 
    "birthstate": player.get("birthStateProvince", "N/A"), "stats": player["stats"]}
    return playerBio

@app.route('/', methods=['GET'])
def index():
    try:
        print("start app")
        teams = get_team_info(get_teams(MLB_domain + team_endpoint))
        return render_template('index.html', teams=teams)
    except Exception as err:
        traceback.print_exc()
        return err, 400

@app.route('/team', methods=['GET'])
def team():
    try:
        id = request.args.get('id')
        team_player = get_player_info(get_team_roster(MLB_domain + single_team + str(id) + roster))
        return render_template('team.html', players=team_player)
    except Exception as err:
        traceback.print_exc()
        return err, 400

@app.route('/player', methods=['GET'])
def playerbio():
    try:
        id = request.args.get('id')
        team_player_bio = get_player_bio(get_player(MLB_domain + player + str(id) + player_bio))
        return render_template('player.html', player=team_player_bio)
    except Exception as err:
        traceback.print_exc()
        return err, 400

if __name__ == "__main__":
    app.run()
